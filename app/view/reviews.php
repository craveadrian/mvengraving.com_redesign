<div id="content">
	<div class="row">
		<h1>Reviews</h1>
		<div class="reviews-box">
			<p class="header"> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> Jul 19, 2017 by Jean on Mountain View Engraving, Inc</p>
			<p>What a wonderful company to do business with. Ben Rosenstein has been in the engraving industry for years. Knows everything there is to know about engraving materials and engraving on any substrate. He is technical and yet personable. He can take any project from inception to the end product with ease. Love his work and professionalism. You won't find a better person to make your customized product look beautiful. Great prices, better than any in the marketplace! I love the special attention he gives to his customers. His engraving comes out beautiful on any material. Give him a call, I know I will call him again! Simply elegant products with great prices.<p>
		</div>
		<div class="reviews-box">
			<p class="header"> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> Jul 19, 2017 by John on Mountain View Engraving, Inc</p>
			<p>This is great work by a great guy. I would highly recommend this company and the price is very reasonable.<p>
		</div>
	</div>
</div>
