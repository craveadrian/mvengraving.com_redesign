<footer>
	<div id="footer">
		<div class="footer-top">
			<div class="row">
				<div class="contact-info">
					<div class="address">
						<h3>LOCATION:</h3>
						<p><?php $this->info("address"); ?></p>
					</div>
					<div class="phone">
						<h3>PHONE:</h3>
						<p><?php $this->info(["phone","tel","footer-phone"]); ?></p>
					</div>
					<div class="email">
						<h3>EMAIL:</h3>
						<p><?php $this->info(["email","mailto","footer-email"]); ?></p>
					</div>
				</div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3269.396775420357!2d-85.31413868476048!3d34.971724980365686!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88605d1f96bdf4ff%3A0xdc63b40d42793c07!2s1325+Wilson+Rd+%2319%2C+Rossville%2C+GA+30741%2C+USA!5e0!3m2!1sen!2sph!4v1557300156261!5m2!1sen!2sph" allowfullscreen></iframe>
			</div>
		</div>
		<?php if($view != "contact"):?>
			<div class="footer-mid">
				<h2>Contact Us</h2>
				<h3>GET IN TOUCH WITH US</h3>
				<div class="row">
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<div class="form-top">
							<label><span class="ctc-hide">Name</span>
								<input type="text" name="name" placeholder="Name:">
							</label>
							<label><span class="ctc-hide">Email</span>
								<input type="text" name="email" placeholder="Email:">
							</label>
							<label><span class="ctc-hide">Phone</span>
								<input type="text" name="phone" placeholder="Phone:">
							</label>
						</div>
						<label><span class="ctc-hide">Message</span>
							<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
						</label>
						<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
						<div class="g-recaptcha"></div>
						<div class="form-bottom">
							<label>
								<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
							</label><br>
							<?php if( $this->siteInfo['policy_link'] ): ?>
							<label>
								<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
							</label>
							<?php endif ?>
						</div>
						<button type="submit" class="ctcBtn btn" disabled>SEND MESSAGE</button>
					</form>
				</div>
			</div>
		<?php endif; ?>
		<div class="footer-bottom">
			<div class="row">
				<div class="left">
					<p class="copy">
						Copyright © <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved.
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
						<?php endif ?>
					</p>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
				</div>
				<div class="right">
					<p class="social-media">
						<a href="<?php $this->info("fb_link"); ?>" target="_blank">F</a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank">L</a>
						<a href="<?php $this->info("fr_link"); ?>" target="_blank">n</a>
						<a href="<?php $this->info("li_link"); ?>" target="_blank">I</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span class="ctc-hide">Call To Action Button</span></a>
</footer>
<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
				$( '.destroy-on-load' ).remove();
			})
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 5 });
		$('#gall2').pajinate({ num_page_links_to_display : 3, items_per_page : 5 });
		$('#gall3').pajinate({ num_page_links_to_display : 3, items_per_page : 5 });
		$('#gall4').pajinate({ num_page_links_to_display : 3, items_per_page : 5 });
		$('#gall5').pajinate({ num_page_links_to_display : 3, items_per_page : 5 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>


<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
