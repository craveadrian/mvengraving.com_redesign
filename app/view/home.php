<div id="company">
	<div class="row">
		<div class="text">
			<h1><?php $this->info("company_name"); ?></h1>

			<p>With experience in the engraving industry since 1986, Mountain View Engraving offers the Quality and Excellence you deserve in all your engraving needs. Located in Rossville, Georgia, our Diamond Drag, Rotary and Laser Engraving Equipment allow us to provide our customers with superior quality & craftsmanship along with competitive pricing on products from stainless steel to glass. We strive for the highest level of commitment to quality and on time delivery.</p>

			<p>From your concept to finished design, Mountain View Engraving strives for the very best. No job is too big or too small. We are your one stop shop to get your engraving needs completed and on time.</p>

			<a href="services#content" class="btn">LEARN MORE</a>
		</div>

		<div class="images">
			<img src="public/images/content/gun1.jpg" alt="Gun 1" class="gun1">
			<img src="public/images/content/gun2.jpg" alt="Gun 2" class="gun2">
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="text">
			<h2>WE OFFER</h2>
			<p>Gift Items, Awards, Acrylic Corporate Award, Interior & Exterior Signage, Personal Identification (Name Badges), Machinery Identification Legend Plates, Commercial & Residential Signs, Industrial Name Plates,<br> Stainless Steel Marking, Parts Marking, Directional Signage, Directory Signs Jewelry Items, Pendants, Pocket Watches, Watch Backs, Trinkets, Rings ,Wine Bottles, Mugs, Yeti Cups, Corporate Logo Design & Engraving</p>
		</div>
		<div class="container">
			<dl>
				<dt>
					<img src="public/images/content/service1.jpg" alt="INDUSTRIAL ENGRAVING">
					<p>INDUSTRIAL ENGRAVING</p>
				</dt>
				<dd>
					<p>We offer a wide variety of services using the latest engraving technology. Specializing in Quick Turnaround Time, Low Cost with Quality Engraving for your Industrial Needs. </p>
					<a href="services#service1" class="btn">LEARN MORE</a>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/content/service2.jpg" alt="JEWELRY ENGRAVING">
					<p>JEWELRY ENGRAVING</p>
				</dt>
				<dd>
					<p>Our expertise in workmanship and engraving on your personal jewelry items gives a lifetime of memories. We are committed to your satisfaction. </p>
					<a href="services#service2" class="btn">LEARN MORE</a>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/content/service3.jpg" alt="CUSTOM ENGRAVING">
					<p>CUSTOM ENGRAVING</p>
				</dt>
				<dd>
					<p>As experts in the custom engraving industry, we take pride in our values and commitments to our customers. We have been recognized for our Excellence in Glass Engraving. From Individual to Corporate we can produce anything on any item.</p>
					<a href="services#service3" class="btn">LEARN MORE</a>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>Our Gallery</h2>
		<div class="container1">
			<img src="public/images/content/gallery1.jpg" alt="Gallery1" class="gallery1">
			<img src="public/images/content/gallery2.jpg" alt="Gallery2" class="gallery2">
			<img src="public/images/content/gallery3.jpg" alt="Gallery3" class="gallery3">
		</div>
		<div class="container2">
			<div class="left">
				<img src="public/images/content/gallery4.jpg" alt="Gallery4" class="gallery4">
				<img src="public/images/content/gallery7.jpg" alt="Gallery7" class="gallery7">
			</div>
			<div class="mid">
				<img src="public/images/content/gallery5.jpg" alt="Gallery5" class="gallery5">
				<img src="public/images/content/gallery8.jpg" alt="Gallery8" class="gallery8">
			</div>
			<div class="right">
				<img src="public/images/content/gallery6.jpg" alt="Gallery6" class="gallery6">
				<img src="public/images/content/gallery9.jpg" alt="Gallery9" class="gallery9">
			</div>
		</div>
		<a href="gallery#content" class="btn">VIEW MORE</a>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<h2>Testimonials</h2>
		<h3>WHAT OUR CLIENT SAY?</h3>
		<p>What a wonderful company to do business with. Ben Rosenstein has been in the engraving industry for years. Knows everything there is to know about engraving materials and engraving on any substrate. He is technical and yet personable. He can take any project from inception to the end product with ease. Love his work and professionalism. You won’t find a better person to make your customized product look beautiful. Great prices, better than any in the marketplace! I love the special attention he gives to his customers. His engraving comes out beautiful on any material. Give him a call, I know I will call him again! Simply elegant products with great prices.</p>
		<p class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
		<p class="author">Leah Glover</p>
	</div>
</div>
