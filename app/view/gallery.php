	<div id="content">
		<div class="row">
			<h1>Gallery</h1>
			<div id="gall1" class="gallery-container">
				<h2>Custom Engraving</h2>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "custom") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/custom/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/custom/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall2" class="gallery-container">
				<h2>Glass Engraving</h2>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "glass") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/glass/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/glass/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall3" class="gallery-container">
				<h2>Industrial Engraving</h2>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "industrial") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/industrial/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/industrial/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall4" class="gallery-container">
				<h2>Jewelry Engraving</h2>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "jewelry") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/jewelry/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/jewelry/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall5" class="gallery-container">
				<h2>Sign Making</h2>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "sign") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/sign/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/sign/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
		</div>
	</div>
